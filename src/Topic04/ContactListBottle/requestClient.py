import json
import requests

BASE_URL = 'http://localhost:9876/'

if __name__ == '__main__':
    print('\n========= Empty contact list ============')
    response = requests.get(BASE_URL + 'all')
    print(response.content.decode())
    print(response.status_code)

    print('\n========= Setting headers ============')
    response = requests.get(BASE_URL + 'all',
                            headers={'Test-Data': 'Yes'})
    print(response.content.decode())

    print('\n========= Get Error 404 ============')
    response = requests.get(BASE_URL + 'contact/1')
    print(response.status_code)

    print('\n========= Post a contact as Form Data ============')
    response = requests.post(BASE_URL + 'contact',
                             data={'name': 'Ivan', 'room': '999'})
    id = response.content.decode()
    print(id)

    print('\n========= Get the posted contact ============')
    response = requests.get(BASE_URL + 'contact/' + id)
    print(response.status_code)
    print(response.content.decode())

    print('\n========= Post a contact as JSON ============')
    response = requests.post(BASE_URL + 'contact',
                             headers={'Content-Type': 'application/json'},
                             json={'name': 'Pavel', 'room': '111'})
    id = response.content.decode()
    print(id)

    print('\n========= Contact list ============')
    response = requests.get(BASE_URL + 'all')
    print(response.content.decode())
    print(response.status_code)

    print('\n========= Response as JSON ============')
    print(repr(response.json()))
    print(str(type(response.json())))
