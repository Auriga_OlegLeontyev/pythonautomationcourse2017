from bottle import run, get, post, request, abort
import json
import uuid

contacts = {}

KEY_NAME = 'name'
KEY_ROOM = 'room'


@get('/')
def hello():
    return 'Home page'


@get('/all')
def list_all_contacts():
    if request.headers.get('Test-Data') is not None:
        return "Hi tester!"
    return json.dumps(contacts, indent=2)


@get('/contact/<id>')
def get_contact(id):
    contact = contacts.get(id, None)
    if contact is None:
        abort(404)
    else:
        return json.dumps(contact, indent=2)


def get_contact_from_form_data(request):
    name = request.forms.get(KEY_NAME)
    room = request.forms.get(KEY_ROOM)
    return name, room


def get_contact_from_json(json_dict):
    if isinstance(json_dict, dict):
        name = json_dict.get(KEY_NAME, None)
        room = json_dict.get(KEY_ROOM, None)
        return name, room
    return None, None


@post('/contact')
def post_contact():
    if request.content_type == 'application/x-www-form-urlencoded':
        name, room = get_contact_from_form_data(request)
    elif request.content_type == 'application/json':
        name, room = get_contact_from_json(request.json)
    else:
        raise ValueError("Bad content type: " + request.content_type)

    if name is not None and room is not None \
            and name.strip() != '' and room.strip() != '':
        id = uuid.uuid4().hex
        contacts[id] = {KEY_NAME: name, KEY_ROOM: room}
        return id

    raise ValueError("Bad request data")


if __name__ == '__main__':
    run(host='localhost', port=9876)
