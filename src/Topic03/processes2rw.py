import os.path
import subprocess

script_path = os.path.join(os.path.dirname(__file__), 'processes2rw_app.py')
command = 'python.exe ' + script_path

process = subprocess.Popen(command,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE)
print('PID: ' + str(process.pid))

for line in ['print 1', 'print 2']:
    input_bytes = (line + '\r\n').encode()
    process.stdin.write(input_bytes)
    process.stdin.flush()
    output_bytes = process.stdout.readline()
    output_line = output_bytes.decode().strip()
    if output_line == line.upper():
        print('Correct line: ' + output_line)
    else:
        print('ERROR: expected line: {}, actual: {}'.format(line, output_line))

process.stdin.write('exit\r\n'.encode())
process.communicate()
process.wait()
print('=== Finished ===')
