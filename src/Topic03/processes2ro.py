import sys
import subprocess

command = 'netstat.exe'
# Can use: None, PIPE, STDOUT, DEVNULL
process = subprocess.Popen('netstat.exe',
                           shell=True,
                           stdout=subprocess.PIPE)
print('PID: ' + str(process.pid))

counter = 0
while True:
    line_data = process.stdout.readline()
    line = line_data.decode().strip()
    if line == '' and process.poll() is not None:
        break
    elif len(line) > 0:
        print('.', end='')
        sys.stdout.flush()  # or use python -u
        counter += 1

print('Lines: ' + str(counter))

process.communicate(timeout=2)
process.wait()
print('=== Finished ===')
