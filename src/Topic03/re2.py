import re

full_pattern = re.compile(r'New folder(?: \(\d{1,2}\))?')
print(full_pattern.fullmatch("New folder") is not None)
print(full_pattern.fullmatch("A New folder") is not None)
print(full_pattern.fullmatch("New folder (2)") is not None)
print(full_pattern.fullmatch("New folder (100)") is not None)

line_pattern = re.compile(r'''^\[(?P<date>\d{4}-\d{2}-\d{2})]\s+
                                 (?P<status>PASS|FAIL)\s+
                                 (?P<code>\d+).*$''', re.X)
m = line_pattern.match("[2017-02-03] PASS 200")
print("date: {}, status: {}, code: {}".format(
    m.group("date"), m.group("status"), m.group("code")))
m = line_pattern.match("[2017-02-03] FAIL 501")
print("date: {}, status: {}, code: {}".format(
    m.group("date"), m.group("status"), m.group("code")))
