import xml.dom.minidom as minidom

import os.path

xml_file_path = os.path.join(os.path.dirname(__file__), 'typical_xml.xml')
xml_doc = minidom.parse(xml_file_path)

# >>> xml_doc.nodeType == minidom.Node.DOCUMENT_NODE
# >>> xml_doc.childNodes
root = xml_doc.childNodes[0]
# >>> root.nodeType == minidom.Node.ELEMENT_NODE
# >>> root.nodeName
# >>> root.childNodes
child_elements = [node for node in root.childNodes if node.nodeType == minidom.Node.ELEMENT_NODE]
# >>> child_elements[2].childNodes
# >>> child_elements[1].childNodes
title01 = child_elements[1].childNodes[1]
# >>> title 01

# get CDATA
# >>> child_elements[1].childNodes[3].childNodes[1]

# >>> title01.attributes
# >>> title01.getAttribute("value")
title01.setAttribute("test", "test")
# >>> title01.toxml()
# >>> root.toprettyxml()
