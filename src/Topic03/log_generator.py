from datetime import datetime, timedelta
from random import randint

possible_errors = [
    'Calculation failed',
    'Incorrect arguments',
    'Timeout exceeded',
    'All server slots busy',
    'Unknown exception',
    'Item not found',
    'I/O Error',
]

num_requests = 128
start_num = 83
error_each_nth = 5
unfinished_each_nth = 13

min_latency = 20
max_latency = 360
min_interval = 2
max_interval = 30

out_file = 'task_1_2.log'


class Types:
    START = 1
    END = 2
    ERROR = 3


class RequestEvent:
    DateFormat = "%Y-%m-%d %H:%M:%S.%f"

    def __init__(self, typ, name, timestamp, error=''):
        self.typ = typ
        self.name = name
        self.timestamp = timestamp
        self.error = error

    def __str__(self):
        if self.typ == Types.ERROR:
            return "[{}] ERROR {} for >{}<".format(
                self.timestamp.strftime(self.DateFormat),
                self.error,
                self.name
            )
        else:
            return "[{}] INFO {} processing of >{}<".format(
                self.timestamp.strftime(self.DateFormat),
                'Started' if self.typ == Types.START else 'Finished',
                self.name
            )


if __name__ == '__main__':
    request_events = []
    cur_time = datetime.now()
    error_delay = timedelta(milliseconds=200)

    for req_no in range(start_num + 1, start_num + num_requests):
        failed = randint(1, error_each_nth) == 1
        finished = randint(1, unfinished_each_nth) != 1
        time_len = timedelta(seconds=randint(min_latency, max_latency),
                             milliseconds=randint(0, 978))
        request_events.append(RequestEvent(
            typ=Types.START,
            name="Request " + str(req_no),
            timestamp=cur_time))

        if failed:
            error_no = randint(1, len(possible_errors)) - 1
            request_events.append(RequestEvent(
                typ=Types.ERROR,
                name="Request " + str(req_no),
                timestamp=cur_time + time_len - error_delay,
                error=possible_errors[error_no]))

        if finished:
            request_events.append(RequestEvent(
                typ=Types.END,
                name="Request " + str(req_no),
                timestamp=cur_time + time_len))

        cur_time += timedelta(seconds=randint(min_interval, max_interval),
                              milliseconds=randint(0, 500))

    with open(out_file, 'w') as fd:
        for line in sorted(request_events, key=lambda x: x.timestamp):
            print(str(line), file=fd)
