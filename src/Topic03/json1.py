import json

obj = json.loads('{"count": 1, "states": [true, false], "details": { "a": null, "b": 118 } }')
print(repr(obj))

array_of_obj = [{"name": "Ivan", "surname": "Ivanov"},
                {"name": "Sergey", "surname": "Petrov"}]
print(repr(json.dumps(array_of_obj)))
