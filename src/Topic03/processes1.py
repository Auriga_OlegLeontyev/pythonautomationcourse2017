import subprocess

process = subprocess.Popen('notepad.exe')
print('PID: ' + str(process.pid))

# process.wait()
# print('=== Finished ===')

try:
    exit_code = process.wait(5)
    print('Finished with code: ' + str(exit_code))
except subprocess.TimeoutExpired:
    print('Did not finish in time')

