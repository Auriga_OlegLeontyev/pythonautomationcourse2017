import json


class B(object):
    def __init__(self):
        self.fieldB1 = "ABC"


class A(object):
    def __init__(self):
        self.fieldA1 = [1, 2, 3]
        self.fieldA2 = B()


class ClassEncoder(json.JSONEncoder):
    def default(self, obj):
        return obj.__dict__


if __name__ == '__main__':
    array_of_A = [A(), A()]
    print(ClassEncoder().encode(array_of_A))
