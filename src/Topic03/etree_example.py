import xml.etree.ElementTree as ET

import os.path

xml_file_path = os.path.join(os.path.dirname(__file__), 'typical_xml.xml')
xml_doc = ET.parse(xml_file_path)
# for string: ET.fromstring(string)

root = xml_doc.getroot()
# >>> root.tag
# >>> root.getchildren()
print(root.tag)
print(root.getchildren())

root.findall('section')
titles = [node.find('title').get('value')
          for node in root.findall('section')]
titles = [node.get('value') for node in root.findall('.//title')]
# >>> titles
print(titles)

intro_node = ET.Element('introduction')
intro_node.set('revision', '1.1')
intro_node.text = "Intro"
root.insert(0, intro_node)

# >>> ET.tostring(root)
print(ET.tostring(root).decode('ascii'))
