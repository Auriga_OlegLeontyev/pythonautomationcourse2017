import os
import multiprocessing


def spawned():
    print("My PID is: " + str(os.getpid()))


if __name__ == "__main__":
    print("Main process PID is: " + str(os.getpid()))
    for i in range(3):
        process = multiprocessing.Process(target=spawned)
        process.start()
        process.join()
