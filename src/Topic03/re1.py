import re


def all_words(line):
    return re.findall(r'\b\w+\b', line)


def extract_float(line):
    numbers = re.findall(r'[+-]?\d+(?:\.\d+)?', line)
    return float(numbers[0]) if len(numbers) > 0 else None


if __name__ == '__main__':
    print(repr(all_words('The quick brown fox')))

    match_obj = re.match(r'^.*\b(\w+)$', "The quick brown fox")
    print(match_obj.group(1))

    line = "The Good, the Bad and the Ugly"
    print(re.sub('Bad|Ugly', 'Good', line))

    print(extract_float("Weight: 35.446 Ar"))
    print(extract_float("Oxidation state: -1"))

    split_pattern = re.compile(r'[\s,;]+')
    line_parts = split_pattern.split('part1,part2 part3; part4')
    print(repr(line_parts))
