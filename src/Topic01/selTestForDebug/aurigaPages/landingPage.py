# -*- coding: utf-8 -*-


class LandingPage(object):
    URL = 'http://auriga.com/'

    def __init__(self, driver):
        self.driver = driver

    def open(self):
        self.driver.get(self.URL)

    def get_language_switcher_RU(self):
        return self.driver.find_element_by_xpath('//div[@id="header_menu"]//a[text()="RU"]')

    def get_contacts_menu_by_link(self):
        return self.driver.find_element_by_xpath('//div[@id="header_menu"]//a[@href="/contact_us"]')

    def get_top_menu_links(self):
        menu_items = self.driver.find_elements_by_xpath('//nav[@class="top-menu"]/ul/li/a')
        return [item.get_text() for item in menu_items]
