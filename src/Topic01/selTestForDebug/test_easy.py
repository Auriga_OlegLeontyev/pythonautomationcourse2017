# -*- coding: utf-8 -*-
from Topic01.selTestForDebug.aurigaPages import LandingPage


def test_switch_to_russian(driver):
    page = LandingPage(driver)
    page.open()
    page.get_language_switcher_RU().click()
    contacts = page.get_contacts_menu_by_link()
    assert contacts.is_displayed(), 'Contacts menu should be visible'
    assert contacts.text == "КОНТАКТЫ", 'Contacts menu should have text "КОНТАКТЫ"'


def test_russian_menu(driver):
    page = LandingPage(driver)
    page.open()
    page.get_language_switcher_RU().click()
    actual_links = page.get_top_menu_links()
    expected_links = ['СЕРВИСЫ И МОДЕЛИ',
                      'ЭКСПЕРТИЗА',
                      'ИСТОРИИ УСПЕХА',
                      'ФОРМУЛА [AU]',
                      'КОМПАНИЯ']
    assert actual_links == expected_links, "Top menu links"
