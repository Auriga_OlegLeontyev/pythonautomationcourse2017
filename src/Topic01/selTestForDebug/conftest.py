from pytest import fixture
from selenium.webdriver import Chrome


@fixture(scope='session')
def driver():
    chrome = Chrome()
    chrome.implicitly_wait(10)
    chrome.maximize_window()
    yield chrome
    chrome.quit()
