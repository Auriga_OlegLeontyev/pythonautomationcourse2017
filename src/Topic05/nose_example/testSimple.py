from nose import with_setup


def setup():
    print("Set Up Module...")


def teardown():
    print("Tear Down Module...")


def setup_for_test():
    print("Set Up Test...")


def teardown_for_test():
    print("Tear Down Test...")


@with_setup(setup_for_test, teardown_for_test)
def test_simple1():
    print("Nose Test 1")
    pass


def test_simple2():
    print("Nose Test 2")
    pass


@with_setup(setup_for_test)
def test_simple3():
    print("Nose Test 3")
    pass
