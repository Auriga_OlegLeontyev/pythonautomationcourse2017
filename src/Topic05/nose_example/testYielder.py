def test_generator():
    for i in [2, 4, 6, 8, 9]:
        yield check_even, i


def check_even(number):
    print('Checking ' + str(number) + '...')
    assert number % 2 == 0
