class TestYielderClass(object):
    def test_generator(self):
        for i in [2, 4, 6, 8, 9]:
            yield self.check_even, i

    def check_even(self, number):
        print('Checking ' + str(number) + '...')
        assert number % 2 == 0
