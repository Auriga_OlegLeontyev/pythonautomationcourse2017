from nose.plugins.attrib import attr


def setup():
    print("Set Up Module...")


def teardown():
    print("Tear Down Module...")


@attr('smoke')
def test_simple1():
    print("Nose Test 1")
    pass


@attr(speed='slow')
def test_simple2():
    print("Nose Test 2")
    pass


@attr('smoke', speed='fast')
def test_simple3():
    print("Nose Test 3")
    pass
