import unittest
import random
from parameterized import parameterized, param


def randomize():
    for i in range(0, 5):
        yield (random.randint(1, 10),)


class TestParameterizedTestCase(unittest.TestCase):
    @parameterized.expand([
        (2, 4),
        (0, 1),
        (3, 9)
    ])
    def test_squares_exp(self, number, expected_result):
        self.assertEquals(number * number, expected_result)

    @parameterized.expand(randomize)
    def test_even(self, number):
        print("Number {0}\n".format(number))
        self.assertEquals(number % 2, 0)

    @parameterized.expand([(line,) for line in open('parameter_feed.txt')])
    def test_eval(self, line):
        print("Exaluating: " + line)
        self.assertTrue(eval(line))
