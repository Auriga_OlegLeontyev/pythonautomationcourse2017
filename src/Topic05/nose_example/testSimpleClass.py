import unittest

import nose.tools


class TestSimpleClass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("Class Setup")

    @classmethod
    def tearDownClass(cls):
        print("Class Teardown")

    def setUp(self):
        print("Test Setup")

    def tearDown(self):
        print("Test Teardown")

    def test1(self):
        print('Test 1')
        self.assertEquals(True, True)

    def test2(self):
        print('Test 2')
        self.assertNotEquals(True, False)

    @nose.tools.nottest
    def test3(self):
        print('Test 3')
        self.assertNotEquals(True, False)

    @nose.tools.istest
    def check3(self):
        print('Check 3')
        self.assertNotEquals(True, False)
