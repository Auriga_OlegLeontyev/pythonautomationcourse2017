@feature_tag
Feature: Demo for Behave 2

  @tag2
  Scenario Outline: Ping test
    When I ping "<host>"
    Then The host is <available>

    Examples:
      | host      | available     |
      | localhost | available     |
      | ********* | not available |
