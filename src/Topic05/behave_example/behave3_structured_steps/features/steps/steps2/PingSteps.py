import os
import sys
from behave import when, then


@when('I ping "{host}"')
def i_ping_host(context, host):
    count_key = 'n' if sys.platform == 'win32' else 'c'
    context.exit_code = os.system("ping -{} 1 {}".format(count_key, host))


@then('The host is available')
def the_host_is_available(context):
    assert context.exit_code == 0


@then('The host is not available')
def the_host_is_not_available(context):
    print("EXIT CODE: {}".format(context.exit_code))
    assert context.exit_code == 1
