from __future__ import print_function


def before_all(context):
    print(" =====  BEFORE ALL  =====")


def after_feature(context, feature):
    print(" =====  Feature '{0}' took {1} seconds to execute  =====".
          format(feature.name, feature.duration))


def before_tag(context, tag):
    print("--> Tag is: " + repr(tag))


def before_scenario(context, scenario):
    print("Scenario name: " + scenario.name)
    print("Scenario tags: " + repr(scenario.tags))


def after_scenario(context, scenario):
    print("===== END OF SCENARIO =====\n\n\n")


def after_step(context, step):
    print("Step: [{0}] from [{1}]".format(step.name, step.filename))
    print("Step status: " + str(step.status))
