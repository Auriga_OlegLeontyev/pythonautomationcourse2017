from __future__ import print_function

from random import randint

from behave import given, when, then, step


@given('I guess a random number')
def i_guess(context):
    context.number = randint(0, 100)


@when('I multiply it by {multiplier}')
def multiply_number(context, multiplier):
    context.product = context.number * int(multiplier)


@then('The product should be even')
def the_product_should_be_even(context):
    print("Product is: {0}".format(context.product))
    assert context.product % 2 == 0
