from bottle import get, post, run, abort
from datetime import datetime, timedelta
from random import randint
import uuid

calculations = {}

KEY_TIMESTAMP = 't'
KEY_VALUE = 'v'

MIN_CALC_TIME = 2
MAX_CALC_TIME = 8


@get('/result/<calc_id>')
def get_result(calc_id):
    result = calculations.get(str(calc_id), None)
    if result is None or result[KEY_TIMESTAMP] > datetime.now():
        return 'N/A'
    else:
        del calculations[calc_id]
        return str(result[KEY_VALUE])


def start_calc(x, y, fun):
    if (not x.isdigit()) or (not y.isdigit()):
        abort(500, "Illegal values: should be integer numbers")

    calc_id = uuid.uuid4().hex
    delta = timedelta(seconds=randint(MIN_CALC_TIME, MAX_CALC_TIME))
    calculations[calc_id] = {KEY_VALUE: fun(int(x), int(y)),
                             KEY_TIMESTAMP: datetime.now() + delta}
    return calc_id


@post('/add/<x>/<y>')
def start_add(x, y):
    return start_calc(x, y, lambda r, l: r + l)


@post('/sub/<x>/<y>')
def start_add(x, y):
    return start_calc(x, y, lambda r, l: r - l)


if __name__ == '__main__':
    run(host='localhost', port='56789')
