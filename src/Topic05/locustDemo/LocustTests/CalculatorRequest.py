from datetime import datetime, timedelta
import requests
import requests.exceptions
import time

from locust import events

DEFAULT_REQUEST_TIMEOUT_IN_MS = 20000
DEFAULT_POLL_INTERVAL_IN_MS = 500

class CalculatorClient(object):
    def __init__(self, host):
        self.host = host
        self.timeout = DEFAULT_REQUEST_TIMEOUT_IN_MS
        self.poll_interval = DEFAULT_POLL_INTERVAL_IN_MS

    def __calc(self, x, y, method):
        start_time = datetime.now()
        end_time = start_time + timedelta(
            seconds=int(self.timeout / 1000),
            microseconds=(self.timeout % 1000) * 1000)

        try:
            total_len = 0

            resp = requests.post("{}/{}/{}/{}".format(
                self.host, method, x, y
            ), timeout=self.timeout)

            calc_id = resp.content.decode()
            total_len += len(calc_id)

            success = True
            while datetime.now() < end_time:
                resp = requests.get("{}/result/{}".format(
                    self.host, calc_id
                ), timeout=self.timeout)
                result = resp.content.decode()
                total_len += len(result)
                if result.isdigit():
                    break
                time.sleep(0.001 * self.poll_interval)
            else:
                success = False

            # TODO: In real test should also assert calc result
            actual_end_time = datetime.now()
            total_time = int((actual_end_time - start_time).total_seconds() * 1000)
            if not success or actual_end_time > end_time:
                events.request_failure.fire(name=method,
                                            request_type="POST",
                                            response_time=total_time,
                                            exception=AssertionError("Timeout exceeded"))
            else:
                events.request_success.fire(name=method,
                                            request_type="POST",
                                            response_length=total_len,
                                            response_time=total_time)

        except Exception as ex:
            actual_end_time = datetime.now()
            total_time = int((actual_end_time - start_time).total_seconds() * 1000)
            events.request_failure.fire(name=method,
                                        request_type="POST",
                                        response_time=total_time,
                                        exception=ex)

    def add(self, x, y):
        self.__calc(x, y, "add")

    def sub(self, x, y):
        self.__calc(x, y, "sub")
