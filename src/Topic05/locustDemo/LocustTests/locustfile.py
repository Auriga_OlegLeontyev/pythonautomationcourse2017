from locust import Locust, TaskSet, task
from random import randint

import sys
import os.path

current_path = os.path.dirname(__file__)
parent_path = os.path.split(current_path)[0]
sys.path.append(parent_path)
from LocustTests.CalculatorRequest import CalculatorClient

MAX_TIMEOUT = 8000
POLL_INTERVAL = 500


class CalcTasks(TaskSet):
    # Waiting between requests for 1 VU
    min_wait = 500
    max_wait = 10000

    @task(10)
    def add(self):
        x = randint(0, 100)
        y = randint(0, 100)
        self.client.add(x, y)

    @task(2)
    def sub(self):
        x = randint(0, 100)
        y = randint(0, 100)
        self.client.sub(x, y)


class MyProfile(Locust):
    host = "http://localhost:56789"
    task_set = CalcTasks

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = CalculatorClient(self.host)
        self.client.timeout = MAX_TIMEOUT
        self.client.poll_interval = POLL_INTERVAL
