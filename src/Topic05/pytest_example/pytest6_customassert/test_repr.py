def test_lists():
    a = [1, 2]
    b = [2, 3, 2]
    assert a == b


def test_sets():
    a = {1, 2}
    b = {1, 2, 3}
    assert a == b
