def pytest_assertrepr_compare(op, left, right):
    if type(left) == list and type(right) == list and op == '==':
        return ['Comparing 2 lists: len={} and len={}'.format(len(left), len(right)),
                '{} != {}'.format(left, right)]
