import threading
import os

def test_1(fix_sess, fix_mod):
    t = threading.current_thread()
    text = 'Test_{0}: SESS:{4}, MOD: {5}, PID: {1}, Thread {2} [{3}]'.format(1, os.getpid(), t.ident, t.name, fix_sess, fix_mod)
    print(text)
    assert text == ""

def test_2(fix_sess, fix_mod):
    t = threading.current_thread()
    text = 'Test_{0}: SESS:{4}, MOD: {5}, PID: {1}, Thread {2} [{3}]'.format(2, os.getpid(), t.ident, t.name, fix_sess, fix_mod)
    print(text)
    assert text == ""

def test_3(fix_sess, fix_mod):
    t = threading.current_thread()
    text = 'Test_{0}: SESS:{4}, MOD: {5}, PID: {1}, Thread {2} [{3}]'.format(3, os.getpid(), t.ident, t.name, fix_sess, fix_mod)
    print(text)
    assert text == ""

def test_4(fix_sess, fix_mod):
    t = threading.current_thread()
    text = 'Test_{0}: SESS:{4}, MOD: {5}, PID: {1}, Thread {2} [{3}]'.format(4, os.getpid(), t.ident, t.name, fix_sess, fix_mod)
    print(text)
    assert text == ""
    
