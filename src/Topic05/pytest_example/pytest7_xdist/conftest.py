import uuid
from pytest import fixture

@fixture(scope='session')
def fix_sess():
    return uuid.uuid4()

@fixture(scope='module')
def fix_mod():
    return uuid.uuid4()

    