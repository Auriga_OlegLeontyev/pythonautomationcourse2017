import pytest

regression = pytest.mark.regression

@pytest.mark.suite1
def test_suite1_1():
    print('Test 1_1')

@regression
@pytest.mark.suite2
def test_suite2_1():
    print('Test 2_1')

@regression
@pytest.mark.suite1
def test_suite1_2():
    print('Test 1_2')

@pytest.mark.suite2
def test_suite2_2():
    print('Test 2_2')

