import os
import pytest

@pytest.mark.skip(reason='Not implemented')
def test_skipped():
    pass

@pytest.mark.skipif(os.getenv('A')!='1', reason='A!=1')
def test_skipif():
    pass

