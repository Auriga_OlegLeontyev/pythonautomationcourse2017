def test_cache1(cache):
    page = cache.get('test_cache/page', None)
    print('Current page is {}'.format(page))
    cache.set('test_cache/page', 'loginPage')


def test_cache2(cache):
    page = cache.get('test_cache/page', None)
    print('Current page is {}'.format(page))
    cache.set('test_cache/page', 'somePage')
