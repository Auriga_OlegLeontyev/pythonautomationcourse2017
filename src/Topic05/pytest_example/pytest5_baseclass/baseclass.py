from pytest import fixture


class BaseClass(object):
    @fixture(autouse=True)
    def base_setup(self):
        print('In setup')
        yield
        print('In teardown')
