def test_fxt1(per_function):
    print('Start test_fxt1')
    print('Fixture data: {}'.format(per_function))


def test_fxt2(per_function_auto, per_module):
    print('Start test_fxt2')
    print('Fixture data: {}'.format(per_function_auto))
    print('Fixture data: {}'.format(per_module))


def test_fxt3(per_module):
    print('Start test_fxt3')
    print('Fixture data: {}'.format(per_module))


def test_fxt_set_status(string_status):
    print('Current string status: ' + string_status.str_value)
    string_status.str_value = 'changed'


def test_fxt_get_status(string_status):
    print('Current string status: ' + string_status.str_value)
