from pytest import fixture
from .my_classes import StringWrapper


@fixture
def per_function():
    print('Init function fixture')
    return 'Data from per_function'


@fixture(autouse=True)
def per_function_auto():
    print('Init automatic function fixture')
    return 'Data from per_function_auto'


@fixture(autouse=True)
def per_function_auto_req(request):
    test_name = request.function.__name__
    print('--- started {} ---'.format(test_name))
    yield
    print('--- stopped {} ---'.format(test_name))


@fixture(scope='module')
def per_module():
    print('Init module fixture')
    return 'Data from per_module'


@fixture(scope='session')
def string_status():
    print('Init string_status fixture')
    return StringWrapper('initial')


@fixture(scope='session', autouse=True)
def setup_and_teardown_session():
    print('=== Init automatic session fixture ===')
    yield 'Session set up'
    print('=== Tear down automatic session fixture ===')
