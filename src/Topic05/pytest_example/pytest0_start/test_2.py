import pytest


@pytest.mark.xfail(raises=ArithmeticError)
def test_02():
    print('02')
    raise ArithmeticError


@pytest.mark.xfail(raises=ArithmeticError)
def test_03():
    print('03')
    raise Exception


if __name__ == "__main__":
    pytest.main(['-s'])
