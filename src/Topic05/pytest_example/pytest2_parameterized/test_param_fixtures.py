import pytest

@pytest.fixture(params=range(0,5))
def seq_id(request):
    return request.param

def test_param1(seq_id):
    print('Seq ID: {}'.format(seq_id))

#######################

@pytest.fixture(params=[(1, 'user1'), (2, 'user2')])
def user_and_id(request):
    return request.param

def test_param2(user_and_id):
    print('User: {0[1]} ID: {0[0]}'.format(user_and_id))

#######################

def get_lines():
    for i in range(0,3):
        yield 'Line{0:03d}'.format(i + 1)

@pytest.fixture(params=get_lines())
def input_line(request):
    return request.param

def test_param3(input_line):
    print('Line was: ' + input_line)

