import pytest


@pytest.mark.parametrize('a, b, result',
                         [(2, 2, 4), (1, -1, 0)],
                         ids=['2_plus_2', '1_plus_neg_1'])
def test_param00(a, b, result):
    assert a + b == result
