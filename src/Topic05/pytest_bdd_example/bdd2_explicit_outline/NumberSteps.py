from __future__ import print_function

from random import randint

from pytest_bdd import given, when, then
from pytest_bdd import parsers

key_guessed_number_product = 'guessed_number_product'


@given('I guess a random number')
def guessed_number():
    return randint(0, 100)


@when(parsers.parse('I multiply it by {multiplier:d}'))
def i_multiply_it_by_n(context, guessed_number, multiplier):
    context[key_guessed_number_product] = guessed_number * int(multiplier)


@then('The product should be even')
def the_product_should_be_even(context):
    product = context[key_guessed_number_product]
    print("Product is: {0}".format(product))
    assert product % 2 == 0
