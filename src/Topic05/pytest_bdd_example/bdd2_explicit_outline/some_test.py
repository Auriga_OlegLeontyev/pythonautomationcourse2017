from __future__ import print_function

from pytest_bdd import scenario

from .NumberSteps import *
from .PingSteps import *


# scenarios('features')

@scenario('features/some.feature', 'Number test')
def test_numbers():
    pass


@scenario(
    'features/some.feature',
    'Ping test',
    example_converters=dict(host=str, available=str)
)
def test_ping():
    pass
