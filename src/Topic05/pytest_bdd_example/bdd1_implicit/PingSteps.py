import os
import sys

from pytest_bdd import when, then

key_exit_code = 'exit_code'


@when('I ping "<host>"')
def ping_result(context, host):
    count_key = 'n' if sys.platform == 'win32' else 'c'
    context[key_exit_code] = os.system("ping -{} 1 {}".format(count_key, host))


@then('The host is <available>')
def the_host_is_available(context, available):
    if available == 'available':
        assert context[key_exit_code] == 0
    elif available == 'not available':
        assert context[key_exit_code] > 0
    else:
        assert False, ("Unknown value: " + available)
