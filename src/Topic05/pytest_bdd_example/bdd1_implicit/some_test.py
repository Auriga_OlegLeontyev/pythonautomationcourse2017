from __future__ import print_function

from pytest_bdd import scenarios

from .NumberSteps import *
from .PingSteps import *

scenarios('features')
