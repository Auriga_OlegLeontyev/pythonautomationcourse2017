@feature_tag
Feature: Demo for Behave

  @tag1
  Scenario: Number test
    Given I guess a random number
    When I multiply it by 2
    Then The product should be even

  @tag2
  Scenario Outline: Ping test
    When I ping "<host>"
    Then The host is <available>

    Examples:
      | host      | available     |
      | localhost | available     |
      | ********* | not available |
