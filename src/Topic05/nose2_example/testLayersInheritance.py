import unittest

"""
Run with '--plugin nose2.plugins.layers'
"""


class OuterLayer:
    @classmethod
    def setUp(cls):
        print('In set up')

    @classmethod
    def tearDown(cls):
        print('In tear down')

    @classmethod
    def testSetUp(cls):
        print('In TEST set up')

    @classmethod
    def testTearDown(cls):
        print('In TEST tear down')


class InnerLayer(OuterLayer):
    @classmethod
    def setUp(cls):
        print('In set up (inner)')

    @classmethod
    def testSetUp(cls):
        print('In TEST set up (inner)')


class TestClass1(unittest.TestCase):
    layer = InnerLayer

    def test1c(self):
        print('In Test 1C')

    def test2c(self):
        print('In Test 2C')
