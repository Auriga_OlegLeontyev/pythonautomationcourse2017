# Errors

from ..mod1 import func1_1
from ..mod1 import func2_2
from ..mod2dir.mod2 import func2_3

if __name__ == '__main__':
    func1_1()
    func2_2()
    func2_3()
