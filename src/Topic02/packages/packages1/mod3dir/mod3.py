from ..mod2dir.mod2 import func2_3


# Still got an error because of relative imports
def func3_1():
    print("func3_1 calls func2_3:")
    func2_3()


if __name__ == '__main__':
    func3_1()
