from ..mod2dir.mod2 import func2_3


# Can only be called when imported from a higher level
def func3_1():
    print("func3_1 calls func2_3:")
    func2_3()


if __name__ == '__main__':
    # Still got an error because of relative imports
    func3_1()
