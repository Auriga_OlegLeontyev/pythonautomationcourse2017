from Topic02.packages.packages3.mod2dir import mod2
from Topic02.packages.packages3.mod2dir.mod2 import func2_2
from Topic02.packages.packages3.mod3dir.mod3 import func3_1

if __name__ == '__main__':
    mod2.func2_1()
    func2_2()
    func3_1()
