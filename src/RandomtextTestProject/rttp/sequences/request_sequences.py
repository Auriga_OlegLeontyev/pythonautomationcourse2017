import random
from rttp.app_requests.RtApiEnums import TextType, TextLineType
from rttp.app_requests.RtApiRequest import RtApiRequest


def randomize_request(request: RtApiRequest):
    request.text_type = random.choice(list(TextType))
    request.line_type = random.choice(list(TextLineType))

    # Случайно выбранные "магические" числа - только для примера
    # В реальности лучше использовать граничные значения
    request.amount = random.randint(1, 20)
    request.words_min = random.randint(1, 20)
    request.words_max = random.randint(request.words_min + 1, 25)

    return request
