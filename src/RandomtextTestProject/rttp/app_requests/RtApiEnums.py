from enum import Enum


class TextType(Enum):
    LOREM = 'lorem'
    GIBBERISH = 'gibberish'


class TextLineType(Enum):
    PARAGRAPH = 'p'
    UNORDERED_LIST = 'ul'
    ORDERED_LIST = 'ol'
