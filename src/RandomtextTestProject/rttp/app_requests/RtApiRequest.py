import posixpath
import requests
from rttp.app_requests.RtApiEnums import TextType, TextLineType
from rttp.app_requests.RtApiResponse import RtApiResponse

DEFAULT_GET_TIMEOUT_IN_SEC = 5


class RtApiRequest():
    def __init__(self, api_address):
        self.api_address = api_address
        self.text_type = TextType.LOREM
        self.line_type = TextLineType.UNORDERED_LIST
        self.amount = 0
        self.words_max = 0
        self.words_min = 0

    def send(self, timeout=DEFAULT_GET_TIMEOUT_IN_SEC) -> RtApiResponse:
        request_path = "{}/{}-{}/{}-{}".format(
            self.text_type.value,
            self.line_type.value, self.amount,
            self.words_min, self.words_max
        )
        request_uri = posixpath.join(self.api_address, request_path)

        print("Requesting URI: " + request_uri)
        response = requests.get(request_uri, timeout=timeout)
        return RtApiResponse(response)
