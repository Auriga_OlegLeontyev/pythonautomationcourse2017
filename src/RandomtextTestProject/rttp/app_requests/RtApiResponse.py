import requests


class RtApiResponse:
    def __init__(self, response: requests.Response):
        assert (response is not None), "Response is not empty"
        self.response = response
        print("Response body: \n" + response.content.decode())

    def verify_response_is_ok(self):
        assert (self.response.status_code == 200), \
            "Response returned HTTP 200 OK"

    def amount(self) -> int:
        j = self.response.json()
        return int(self.response.json()['amount'])

    def verify_amount(self, expected_amount: int):
        assert self.amount() == expected_amount, \
            "Amount of lines should be: {}".format(expected_amount)
