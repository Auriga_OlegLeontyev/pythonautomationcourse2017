import posixpath
from pytest import fixture
from rttp.settings import TEST_HOST


@fixture(scope='session')
def api_path():
    return posixpath.join(TEST_HOST, 'api')
