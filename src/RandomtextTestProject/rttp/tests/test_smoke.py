from rttp.app_requests.RtApiRequest import RtApiRequest
from rttp.sequences.request_sequences import randomize_request


def test_01_amount(api_path: str):
    request = RtApiRequest(api_path)
    randomize_request(request)

    expected_amount = 7
    request.amount = expected_amount

    response = request.send()
    response.verify_response_is_ok()
    response.verify_amount(expected_amount)
